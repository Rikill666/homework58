import React, {Component} from 'react';
import Modal from "./components/UI/Modal/Modal";
import Button from "./components/UI/Button/Button";
import Alert from "./components/UI/Alert/Alert";

class App extends Component {
    state = {
        isOpenModal: false,
        isOpenAlert: true
    };
    openModal = () => {
        this.setState({isOpenModal: true});
    };
    chanelModal = () => {
        this.setState({isOpenModal: false});
    };
    continueModal = () => {
        alert('You continued!');
    };

    chanelAlert = () => {
        this.setState({isOpenAlert: false});
    };

    render() {
        return (
            <div>
                <Button
                    type="Danger"
                    onClick={this.openModal}
                    label="Open Modal"
                >
                    Open Modal
                </Button>
                <Modal
                    show={this.state.isOpenModal}
                    close={this.chanelModal}
                    title="Some kinda modal title"
                    buttons ={[{type: 'Success', label: 'Continue', clicked: this.continueModal}, {
                    type: 'Danger',
                    label: 'Close',
                    clicked: this.chanelModal
                }]}
                >
                    <p>This is modal content</p>
                </Modal>
                <Alert
                    show={this.state.isOpenAlert}
                    type="primary"
                    dismiss={this.chanelAlert}
                >
                    This is a warning type alert
                </Alert>
            </div>
        );
    }
}

export default App;