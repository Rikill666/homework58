import React, {Fragment} from 'react';
import './Alert.css'

const Alert = ({type, dismiss, children, show}) => {
    const isButton = dismiss != null ? "inline-block" : "none";
    return (
        <Fragment>
            <div className={['Alert', type].join(' ')} style={{
                transform: show ? 'translateY(0)' : 'translateY(-100vh)',
                opacity: show ? '2' : '0'
            }}
            >
                {children}
                <i onClick={dismiss} style={{display: isButton, marginLeft: '50%'}} className="fas fa-times"/>
            </div>

        </Fragment>
    )
};

export default Alert;