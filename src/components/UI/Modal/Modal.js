import React, {Fragment} from 'react';

import './Modal.css';
import Backdrop from "../Backdrop/Backdrop";
import Button from "../Button/Button";

const Modal = props => {
  return (
    <Fragment>
      <div className="Modal" style={{
        transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
        opacity: props.show ? '2' : '0'
      }}>
          <h3>{props.title}</h3>
          {props.children}
          {props.buttons.map(btn => (
              <Button key={btn.type} type={btn.type} onClick={btn.clicked} label={btn.label}/>
          ))}

      </div>
      <Backdrop show={props.show} onClick={props.close} />
    </Fragment>
  );
};

export default Modal;